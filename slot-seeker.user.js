// ==UserScript==
// @name Slot Seeker
// @version 3.0.3
// @description Refreshes the Amazon Fresh slot selection page until it detects an available slot.
// @author Bob D'Ercole
// @namespace https://bobdercole.com/
// @match https://www.amazon.com/gp/buy/shipoptionselect/handlers/*
// @grant none
// ==/UserScript==

(function() {
	'use strict';

	const slotKeywords = [
		'AM',
		'PM'
	];
	const outOfStockKeywords = [
		'sorry',
		'fulfill'
	];

	const reloadMinimumDelay = 30;
	const reloadMaximumDelay = 60;
	const searchDelay = 5;
	const invertDelay = 1;

	const reloadDelay = Math.floor(Math.random() * (reloadMaximumDelay - reloadMinimumDelay + 1)) + reloadMinimumDelay;
	const keywords = [...slotKeywords, ...outOfStockKeywords];

	function requestNotificationPermission() {
		if ('Notification' in window === false) {
			return;
		}

		Notification.requestPermission();
	}

	function createNotification() {
		if ('Notification' in window === false || Notification.permission !== 'granted') {
			return;
		}

		new Notification('Slot Seeker', {
			'requireInteraction': true,
			'vibrate': true,
			'body': 'Your Amazon order requires attention.'
		});
	}

	function notify() {
		let invert = 0;
		const invertIntervalId = setInterval(() => {
			invert = 100 - invert;
			document.body.style.filter = `invert(${invert}%)`;
		}, invertDelay * 1000);

		createNotification();

		document.addEventListener('click', () => {
			clearInterval(invertIntervalId);
			document.body.style.filter = 'invert(0%)';
		}, {
			'once': true
		});
	}

	function keywordExists(keyword) {
		const regex = new RegExp(keyword, 'i');

		if(regex.test(document.body.innerText) === false) {
			return false;
		}

		return true;
	}

	requestNotificationPermission();

	const reloadTimeoutId = setTimeout(() => {
		location.reload();
	}, reloadDelay * 1000);

	const searchIntervalId = setInterval(() => {
		for (const keyword of keywords) {
			if (keywordExists(keyword) === false) {
				continue;
			}

			clearTimeout(reloadTimeoutId);
			clearInterval(searchIntervalId);
			notify();
			break;
		}
	}, searchDelay * 1000);
})();
