# Slot Seeker

This script automatically refreshes the Amazon Fresh slot selection page until it detects an available slot. If it finds a slot, it will emit a notification and strobe the page colors.

1. Install [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo).
2. Install [Slot Seeker](https://gitlab.com/bobdercole/slot-seeker/raw/master/slot-seeker.user.js).
3. Checkout your Amazon Fresh cart.
4. Manually refresh the "Schedule your order" page once.
5. Allow notifications if you are prompted.
6. The script should now take over.

You should also verify your operating system notification settings. For example, Windows 10 blocks notifications via Focus Assist by default while gaming. You may need to adjust your settings to ensure that you don't miss a Slot Seeker notification.
